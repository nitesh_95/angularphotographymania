import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './admin/welcome/welcome.component';
import { WhoAmIComponent } from './admin/who-am-i/who-am-i.component';
import { BdayComponent } from './admin/bday/bday.component';
import { BlogsComponent } from './admin/blogs/blogs.component';
import { CelebrityComponent } from './admin/celebrity/celebrity.component';
import { CommercialComponent } from './admin/commercial/commercial.component';
import { ContactComponent } from './admin/contact/contact.component';
import { FashionFemaleComponent } from './admin/fashion-female/fashion-female.component';
import { FashionMaleComponent } from './admin/fashion-male/fashion-male.component';
import {  WeddingComponent} from './admin/wedding/wedding.component';
import {SlidermenuComponent} from './admin/slidermenu/slidermenu.component'
import {AdminManiaComponent} from './admin/admin-mania/admin-mania.component'
import{UploadingComponent} from './admin/uploading/uploading.component'
import{GalleryComponent} from './admin/gallery/gallery.component'
import { ContactAllComponent } from './admin/contact-all/contact-all.component';
import { BlogsNewComponent } from './blogs-new/blogs-new.component';
import { PreweddingComponent } from './admin/prewedding/prewedding.component';

const appRoutes: Routes = [
  { path: '', redirectTo: "welcome", pathMatch: "full" },
  {path: 'welcome',component:WelcomeComponent},
  {path: 'whoAmI',component:WhoAmIComponent},
  {path: 'slider',component:SlidermenuComponent},
  {path: 'bday',component:BdayComponent},
  {path: 'blogs',component:BlogsComponent},
  {path: 'celebrity',component:CelebrityComponent},
  {path: 'contact',component:ContactComponent},
  {path: 'commercial',component:CommercialComponent},
  {path: 'fashionFemale',component:FashionFemaleComponent},
  {path: 'fashionMale',component:FashionMaleComponent},
  {path: 'Wedding',component:WeddingComponent},
  {path: 'admin',component:AdminManiaComponent},
  {path: 'upload',component:UploadingComponent},
  {path: 'gallery',component:GalleryComponent},
  {path:'contacts',component:ContactAllComponent},
  {path:'blogsContent',component:BlogsNewComponent},
  {path:'prewedding',component:PreweddingComponent}

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
