import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminManiaComponent } from './admin-mania.component';

describe('AdminManiaComponent', () => {
  let component: AdminManiaComponent;
  let fixture: ComponentFixture<AdminManiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminManiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminManiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
