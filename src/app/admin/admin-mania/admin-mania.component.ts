import { Component, OnInit } from '@angular/core';
import{Router} from '@angular/router'
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Component({
  selector: 'app-admin-mania',
  templateUrl: './admin-mania.component.html',
  styleUrls: ['./admin-mania.component.css']
})
export class AdminManiaComponent implements OnInit {
username:any
password:any
passsword:any
newwpassword:any
confirmpassword:any

  constructor(private router:Router,private http:HttpClient) { }

  ngOnInit() {
  }
toggle(event){
  this.router.navigate(['/upload'])

}
checkPassword(event) {
  // let oldPassword = event.target.form.elements[0].value;
  // let newPassword = event.target.form.elements[1].value;
  // let confirmPassword = event.target.form.elements[2].value
  if (this.passsword == '' || this.newwpassword == '' || this.confirmpassword == '') {
    alert("Please enter all the fields");
  } else if (this.passsword  == this.newwpassword) {
    alert("Old and new password are same")
    return false;
  } else if (this.newwpassword != this.confirmpassword) {
    alert("New and Confirm Password do not match")
  } else {
    let olddPassword = this.passsword;
    let newwPassword = this.confirmpassword
    var solid = this.username
    const base_URL = 'http://localhost:8887/changepassword?Solid=' + solid + '&oldPassword=' + olddPassword + '&newPassword=' + newwPassword;
    return this.http.post(base_URL, {
    }).subscribe(data => {
      if (data['status'] == '01') {
        alert("Password not changed.Something went wrong please Try Again");
      } else if (data['status'] == '00') {
        alert("Password changed successfully. Please Login Again");
        window.location.href = "/login"
      } else {
        alert("Something went wrong Please try again")
      }
    })
}
}


checkLogin(event) {
  if (this.username == '') {
    alert('please enter user ID')
  } else if (this.password == '') {
    alert('please enter password')
  } else {
    const base_URL = 'http://localhost:8887/login?UserId=' + this.username + '&Password=' + this.password;
    return this.http.post(base_URL, {}).subscribe(data => {
      if (data['status'] == '00' ) {
     alert("Welcome Admin")
     this.router.navigate(['/upload'])
      }else{
        alert("Wrong Username or Password")
        window.location.reload()
      }
    })
  }
}
}
