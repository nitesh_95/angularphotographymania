import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ThemeService } from 'ng2-charts';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  approvedApplications : any = []
  file:any
  buttonValue:any
  buttonValues:any
  constructor( private http:HttpClient,private router:Router) { }

  ngOnInit() {
    // this.toggle2(event)
    // this.toggle3(event)
    // this.toggle4(event)
    // this.toggle5(event)
    // this.toggle6(event)
    // this.toggle7(event)
    $(function() {
      var selectedClass = "";
      $(".filter").click(function(){
      selectedClass = $(this).attr("data-rel");
      $("#gallery").fadeTo(100, 0.1);
      $("#gallery div").not("."+selectedClass).fadeOut().removeClass('animation');
      setTimeout(function() {
      $("."+selectedClass).fadeIn().addClass('animation');
      $("#gallery").fadeTo(300, 1);
      }, 300);
      });
      });
  }

  toggle1(event){
    this.router.navigate(["/bday"])
  }


  toggle2(event){
    this.router.navigate(["/celebrity"])
  }

toggle3(event) {
  this.router.navigate(["/commercial"])
}
  toggle4(event) {
    this.router.navigate(["/fashionMale"])
  }
    toggle5(event) {
      this.router.navigate(["/fashionFemale"])
    }
  
        toggle6(event) {
          this.router.navigate(["/fashionFemale"])
        }
        toggle7(event) {
          this.router.navigate(["/Wedding"])
        }
  }