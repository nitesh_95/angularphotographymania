import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FashionFemaleComponent } from './fashion-female.component';

describe('FashionFemaleComponent', () => {
  let component: FashionFemaleComponent;
  let fixture: ComponentFixture<FashionFemaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FashionFemaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FashionFemaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
