import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wedding',
  templateUrl: './wedding.component.html',
  styleUrls: ['./wedding.component.css']
})
export class WeddingComponent implements OnInit {

  approvedApplications:any= []
  file:any
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.clicked(event)
  }
  toggle1(event){
    this.router.navigate(["/bday"])
  }
  toggle8(event){
    this.router.navigate(["/prewedding"])
  }


  toggle2(event){
    this.router.navigate(["/celebrity"])
  }

toggle3(event) {
  this.router.navigate(["/commercial"])
}
  toggle4(event) {
    this.router.navigate(["/fashionMale"])
  }
    toggle5(event) {
      this.router.navigate(["/fashionFemale"])
    }
  
        toggle6(event) {
          this.router.navigate(["/fashionFemale"])
        }
        toggle7(event) {
          this.router.navigate(["/Wedding"])
        }
  clicked(event) {
    const base_URL = 'http://localhost:8020/getPhotoPath' 
    return this.http.post(base_URL, {
      photoId:6
    }).subscribe(data => {
      this.approvedApplications.push(data)
      this.approvedApplications = this.approvedApplications[0]
      this.approvedApplications[0]=data['imagesPath']
      this.file= this.approvedApplications[0]
      console.log(data['imagesPath'])
    })
  }}