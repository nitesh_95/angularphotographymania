import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidermenuComponent } from './slidermenu.component';

describe('SlidermenuComponent', () => {
  let component: SlidermenuComponent;
  let fixture: ComponentFixture<SlidermenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlidermenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlidermenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
