import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
postid:any
title:any
post:any
author:any
date:any
id:any
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
  }
  updateBlog(event) {
    const base_URL = 'http://localhost:8880/blogs/update';
    return this.http.post(base_URL, {
        id:this.id,
        postId:this.postid,
        title:this.title,
        post:this.post,
        author:this.author,
        date:this.date
    }).subscribe(data => {
     alert("Blog has been Posted Successfully")
    window.location.reload()
    })
}

  saveBlog(event) {
      const base_URL = 'http://localhost:8880/blogs/saveAll';
      return this.http.post(base_URL, {
          postId:this.postid,
          title:this.title,
          post:this.post,
          author:this.author,
          date:this.date
      }).subscribe(data => {
        if (data['status'] == '00' ) {
       alert("Blog has been Posted Successfully")
       window.location.reload()
        }else{
          alert("Something Went Wrong")
          window.location.reload()
        }
      })
    }
  }