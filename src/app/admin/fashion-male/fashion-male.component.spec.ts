import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FashionMaleComponent } from './fashion-male.component';

describe('FashionMaleComponent', () => {
  let component: FashionMaleComponent;
  let fixture: ComponentFixture<FashionMaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FashionMaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FashionMaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
