import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
name:any
mobileNo:any
address:any
remarks:any
email:any
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
  }
  sent(event){
    window.location.reload();
  }
  saveData(event){
      const base_URL = 'http://localhost:8888/save'
      return this.http.post(base_URL, {
          name:this.name,
          mobile:this.mobileNo,
          address:this.address,
          remarks:this.remarks,
          email:this.email
      }).subscribe(data => {
        if(data['status']=="00"){
          $('#notInterested').modal('toggle')
          console.log(data)
        }else{
          $('#notInteresteds').modal('toggle')
        }
     
      })
    }
  }
