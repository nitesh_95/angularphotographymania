import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-celebrity',
  templateUrl: './celebrity.component.html',
  styleUrls: ['./celebrity.component.css']
})
export class CelebrityComponent implements OnInit {
  approvedApplications:any= []
  file:any
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.celebrityValue(event)
  }
  toggle1(event){
    this.router.navigate(["/bday"])
  }


  toggle2(event){
    this.router.navigate(["/celebrity"])
  }

toggle3(event) {
  this.router.navigate(["/commercial"])
}
  toggle4(event) {
    this.router.navigate(["/fashionMale"])
  }
    toggle5(event) {
      this.router.navigate(["/fashionFemale"])
    }
    toggle8(event){
      this.router.navigate(["/prewedding"])
    }
  
        toggle6(event) {
          this.router.navigate(["/fashionFemale"])
        }
        toggle7(event) {
          this.router.navigate(["/Wedding"])
        }
  celebrityValue(event) {
    const base_URL = 'http://localhost:8020/getPhotoPath' 
    return this.http.post(base_URL, {
      photoId:6
    }).subscribe(data => {
      this.approvedApplications.push(data)
      this.approvedApplications = this.approvedApplications[0]
      this.approvedApplications[0]=data['imagesPath']
      this.file= this.approvedApplications[0]
      console.log(data['imagesPath'])
    })
  }}
