import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-all',
  templateUrl: './contact-all.component.html',
  styleUrls: ['./contact-all.component.css']
})
export class ContactAllComponent implements OnInit {
  approvedApplications: any = []
  searchText;
  p: number = 1;
  public pageSize: number = 10;
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.getData(event)
  }

  getData(event) {
    const base_url = 'http://localhost:8888/contact/all'
    this.http.post(base_url, {
    }).subscribe((data) => {
      this.approvedApplications.push(data)
      this.approvedApplications = this.approvedApplications[0]
      console.log(data)
    })
  }
}
