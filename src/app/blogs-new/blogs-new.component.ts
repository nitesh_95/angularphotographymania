import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blogs-new',
  templateUrl: './blogs-new.component.html',
  styleUrls: ['./blogs-new.component.css']
})
export class BlogsNewComponent implements OnInit {
  approvedApplications:any = []
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {
    this.getData(event)
  }
  getData(event) {
    const base_url = 'http://localhost:8880/blogs/getAll'
    this.http.post(base_url, {
    }).subscribe((data) => {
      this.approvedApplications.push(data)
      this.approvedApplications = this.approvedApplications[0]
      console.log(data)
    })
  }
}

