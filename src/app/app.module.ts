import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { SliderComponent } from './admin/slider/slider.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { AngularDateTimePickerModule} from 'angular2-datetimepicker';
import {NgxPaginationModule} from 'ngx-pagination'; 
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UserIdleModule } from 'angular-user-idle';
import { MyDatePickerModule } from 'mydatepicker';
import { ChartsModule } from 'ng2-charts';
import { WelcomeComponent } from './admin/welcome/welcome.component';
import { ContactComponent } from './admin/contact/contact.component';
import { BlogsComponent } from './admin/blogs/blogs.component';
import { WhoAmIComponent } from './admin/who-am-i/who-am-i.component';
import { BdayComponent } from './admin/bday/bday.component';
import { WeddingComponent } from './admin/wedding/wedding.component';
import { CelebrityComponent } from './admin/celebrity/celebrity.component';
import { FashionMaleComponent } from './admin/fashion-male/fashion-male.component';
import { FashionFemaleComponent } from './admin/fashion-female/fashion-female.component';
import { CommercialComponent } from './admin/commercial/commercial.component';
import { SlidermenuComponent } from './admin/slidermenu/slidermenu.component';
import { AdminManiaComponent } from './admin/admin-mania/admin-mania.component';
import { GalleryComponent } from './admin/gallery/gallery.component';
import { UploadingComponent } from './admin/uploading/uploading.component';
import { ContactAllComponent } from './admin/contact-all/contact-all.component';
import { BlogsNewComponent } from './blogs-new/blogs-new.component';
import { PreweddingComponent } from './admin/prewedding/prewedding.component';
@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    SliderComponent,
    WelcomeComponent,
    ContactComponent,
    BlogsComponent,
    WhoAmIComponent,
    BdayComponent,
    WeddingComponent,
    CelebrityComponent,
    FashionMaleComponent,
    FashionFemaleComponent,
    CommercialComponent,
    SlidermenuComponent,
    AdminManiaComponent,
    GalleryComponent,
    UploadingComponent,
    ContactAllComponent,
    BlogsNewComponent,
    PreweddingComponent,
  ],
  imports: [
    MyDatePickerModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    Ng2OrderModule,
    NgxPaginationModule,
    FormsModule,
    RouterModule, 
    AngularDateTimePickerModule,
    ChartsModule,
     UserIdleModule.forRoot({idle: 27000, timeout: 27000, ping: 27000})
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
		CUSTOM_ELEMENTS_SCHEMA,
		NO_ERRORS_SCHEMA
	]
})
export class AppModule {
  
 }
